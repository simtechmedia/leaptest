package com.simtechmedia.leaptest 
{
	import flash.geom.Rectangle;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.extensions.camera.StarlingCamera;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;

	import com.leapmotion.leap.Vector3;

	import flash.ui.Keyboard;

	public class Main extends Sprite
	{
		
		
		[Embed(source="/orangeCircle.png")]
		private var circleBMP : Class;
		
		private var circleTex : Texture ;
		
		private var fingersImage : Vector.<Image>;
		
		// Class for holidng leap data
		private var leapService : LeapService ;
		
		private var debugText : TextField;
		
		private var x0 : Number = -80;
		private var x1 : Number = 130; 
		
		private var y0 : Number = 83;
		private var y1 : Number = 350; 
		
		private static const X0_S : String = "x0";
		private static const X1_S : String = "x1";
		private static const Y0_S : String = "y0";
		private static const Y1_S : String = "y1";
		private var selectedPoint : String;
		
		private var camera:StarlingCamera;

		public function Main()
		{
			addEventListener(Event.ADDED_TO_STAGE, onInit);
			
		}

		private function onInit(event : Event) : void
		{
			
			camera = new StarlingCamera();

            //Initialize. Pass in:
            //1. a rect to define the camera "viewport"
            //2. a capture rate (fps, default 24)
            //3. a downsample value (.5 means half-height, half-width, default 1)
            //4. true if you want to rotate the camera (default false)
            camera.init(new Rectangle(0, 0, 1280, 1024), 15, 0.5, false);
            //Each time you call reflect() you toggle mirroring on/off
            camera.reflect();
            //Put it onstage
            addChild(camera);
            //Select a webcam
            camera.selectCamera(0);
			
			selectedPoint = Main.X0_S;
			circleTex = Texture.fromBitmap(new circleBMP());
			
			// Fingers Array
			fingersImage = new Vector.<Image>();
			
			for (var i : int = 0; i < 5; i++) {
				
				var finger : Image = new Image(circleTex);
				addChild(finger);
				fingersImage.push(finger);
				
			}
			debugText = new TextField(500, 500, "DebugField");
			debugText.x = 500;
			debugText.hAlign = HAlign.LEFT;
			addChild(debugText);
			
			// Start Leap Service
			leapService = new LeapService();
			leapService.fingerPositionsSignal.add(fingerDetectedHandler);
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyPress);
		}

		private function onKeyPress(event : KeyboardEvent) : void
		{
			var modifier : Number  = 0 ;
			
			switch(event.keyCode){
				case Keyboard.NUMPAD_7:
					selectedPoint = X0_S;
					break;
				case Keyboard.NUMPAD_9:
					selectedPoint = X1_S;
					break;
				case Keyboard.NUMPAD_1:
					selectedPoint = Y0_S;
					break;
				case Keyboard.NUMPAD_3:
					selectedPoint = Y1_S;
					break;
				case Keyboard.NUMPAD_4 :
					modifier = -1;
					break;
				case Keyboard.NUMPAD_6 :
					modifier = 1;
					break;
			}
			
			switch(selectedPoint){
				case X0_S:
					x0 += modifier;
					break;
				case X1_S:
					x1 += modifier;
					break;
				case Y0_S:
					y0 += modifier;
					break;
				case Y1_S:
					y1 += modifier;
					break;
			}
			
			debugText.text = "x0 : " + x0 + "\n"; ;
			debugText.text += "x1 : " + x1 + "\n"; ;
			debugText.text += "y0 : " + y0 + "\n"; ;
			debugText.text += "y1 : " + y1 + "\n"; ;
		}

		private function fingerDetectedHandler( fingersVector : Vector.<Vector3> ) : void {
			
//			trace("fingerDetectedHandler : " + fingersVector.length);
//			debugText.text = "";
//			trace('fingersVector.length' + fingersVector.length );

			for (var i : int = 0; i < fingersImage.length; i++) {
				
				var fingerImage : Image = fingersImage[i];
				if( i < fingersVector.length )
				{
					fingerImage.visible = true;
//					fingerImage.x = ( fingersVector[i].x - x0 ) / x1 * stage.stageWidth;
					fingerImage.x = ( (fingersVector[i].x -x0 ) / ( x1 - x0 ) )  * stage.stageWidth ;
//					fingerImage.y = stage.stageHeight - ( ( fingersVector[i].y - y0 ) / y1 * stage.stageHeight );
					//fingerImage.y = stage.stageHeight - ( (y1 - y0 ) / ( fingersVector[i].y - y0 ) * stage.stageHeight ) ;
					fingerImage.y =  stage.stageHeight - ( ( (fingersVector[i].y -y0 ) / ( y1 - y0 ) )  * stage.stageHeight );
					fingersVector[i].z < 0 ? fingerImage.alpha = 1 : fingerImage.alpha = 0.25; 
//					trace("fingerImage.y  : " + fingerImage.y );
//					trace("fingersVector.x  : " + fingersVector[i].x );
//					trace("fingerImage.x  : " + fingerImage.x );
					
				} else {
					fingerImage.visible = false;
				}
			}
		}		
	}
}