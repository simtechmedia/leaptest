package com.simtechmedia.leaptest
{
	import starling.core.Starling;
	import starling.utils.HAlign;
	import starling.utils.VAlign;

	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.ui.ContextMenu;
	

	[SWF(width="1280",height="1024",frameRate="60",backgroundColor="#2f2f2f")]
	// [Frame( factoryClass="feathers.examples.componentsExplorer.Main" )]
	public class LeapTest extends MovieClip {
		public function LeapTest()
		{
			
			var menu:ContextMenu = new ContextMenu();
			menu.hideBuiltInItems();
			this.contextMenu = menu;
			
			if(this.stage)
			{
				this.stage.align = StageAlign.TOP_LEFT;
				this.stage.scaleMode = StageScaleMode.NO_SCALE;
			}
			
			this.loaderInfo.addEventListener(Event.COMPLETE, loaderInfo_completeHandler);
		}
		
		private var _starling:Starling;
		
		private function start():void
		{
			this.gotoAndStop(2);
			this.graphics.clear();
			
			Starling.handleLostContext = true;
			Starling.multitouchEnabled = true;
			
			this._starling = new Starling( Main , this.stage );
			this._starling.showStats = true;
			this._starling.showStatsAt(HAlign.LEFT, VAlign.BOTTOM);
			this._starling.start();
		}
		
		private function loaderInfo_completeHandler(event:Event):void
		{
			this.start();
		}
	}
}