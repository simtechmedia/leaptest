package com.simtechmedia.leaptest {
	import com.leapmotion.leap.Finger;
	import com.leapmotion.leap.Frame;
	import com.leapmotion.leap.Hand;
	import com.leapmotion.leap.LeapMotion;
	import com.leapmotion.leap.Vector3;
	import com.leapmotion.leap.events.LeapEvent;
	import com.leapmotion.leap.util.LeapMath;

	import org.osflash.signals.Signal;
	/**
	 * @author SimonLappy
	 */
	public class LeapService 
	{
		// Dispatches all finger vectors ( up to 5 ) ;
		public var fingerPositionsSignal : Signal = new Signal();
		
		private var leap : LeapMotion;
		
		public function LeapService() 
		{
			leap = new LeapMotion();
			leap.controller.addEventListener(LeapEvent.LEAPMOTION_INIT, onInit);
			leap.controller.addEventListener(LeapEvent.LEAPMOTION_CONNECTED, onConnect);
			leap.controller.addEventListener(LeapEvent.LEAPMOTION_DISCONNECTED, onDisconnect);
			leap.controller.addEventListener(LeapEvent.LEAPMOTION_EXIT, onExit);
			leap.controller.addEventListener(LeapEvent.LEAPMOTION_FRAME, onFrame);
		}
		
		private function onInit(event : LeapEvent) : void
		{
			trace("Initialized");
		}

		private function onConnect(event : LeapEvent) : void
		{
			trace("Connected");
		}

		private function onDisconnect(event : LeapEvent) : void
		{
			trace("Disconnected");
		}

		private function onExit(event : LeapEvent) : void
		{
			trace("Exited");
		}

		private function onFrame(event : LeapEvent) : void
		{
			// Get the most recent frame and report some basic information
			var frame : Frame = event.frame;
//			trace("Frame id: " + frame.id + ", timestamp: " + frame.timestamp + ", hands: " + frame.hands.length + ", fingers: " + frame.fingers.length + ", tools: " + frame.tools.length);
			
			var fingersAr : Vector.<Vector3> = new Vector.<Vector3>(); 
			
			if ( frame.hands.length > 0 )
			{
				
				// Get the first hand
				var hand : Hand = frame.hands[ 0 ];

				// Check if the hand has any fingers
//				var fingers : Vector.<Finger> = hand.fingers;
				var fingers : Vector.<Finger> = frame.fingers;
								
				if ( !fingers.length == 0 )
				{
					
					// Calculate the hand's average finger tip position
					var avgPos : Vector3 = Vector3.zero();
					for each ( var finger:Finger in fingers ) {
						avgPos = avgPos.plus(finger.tipPosition);
						fingersAr.push(finger.tipPosition);
//						trace(finger.tipPosition.x);
					}

					avgPos = avgPos.divide(fingers.length);
//					trace("Hand has " + fingers.length + " fingers, average finger tip position: " + avgPos);
					
					fingerPositionsSignal.dispatch(fingersAr);
				}

				// Get the hand's sphere radius and palm position
//				trace("Hand sphere radius: " + hand.sphereRadius + " mm, palm position: " + hand.palmPosition);

				// Get the hand's normal vector and direction
				var normal : Vector3 = hand.palmNormal;
				var direction : Vector3 = hand.direction;

				// Calculate the hand's pitch, roll, and yaw angles
//				trace("Hand pitch: " + LeapMath.toDegrees(direction.pitch) + " degrees, " + "roll: " + LeapMath.toDegrees(normal.roll) + " degrees, " + "yaw: " + LeapMath.toDegrees(direction.yaw) + " degrees\n");
				
				
			}
		}

	}
}
